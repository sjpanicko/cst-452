package com.shoppingcart.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ShoppingCartDaoImpl{

//	CREATE TABLE `user_mstr` ( 
//	`userName` VARCHAR(50) NOT NULL , `password` VARCHAR(50) NOT NULL , `name` VARCHAR(50) NOT NULL , `address` VARCHAR(100) NOT NULL , `phone` VARCHAR(20) NOT NULL , `email` VARCHAR(50) NOT NULL , `created_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ) ;
//	CREATE TABLE `order_mstr` ( `name` VARCHAR(50) NOT NULL , `card_number` INT(16) NOT NULL , `expiration_date` VARCHAR(20) NOT NULL , `cvc` INT(4) NOT NULL , `item_list` VARCHAR(4000) NOT NULL , `created_dt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ) ;

	public boolean registerUser(String userName, String password, String name, String address, String phone, String email) {
		Connection con = DBConnection.getConnection();
		PreparedStatement prepStmt = null;
		ResultSet res = null;
		try {
			prepStmt = con.prepareStatement("insert into user_mstr(userName,password,name,address,phone,email) values(?,?,?,?,?,?)");

			prepStmt.setString(1,userName);  
			prepStmt.setString(2,password);  
			prepStmt.setString(3,name);  
			prepStmt.setString(4,address);  
			prepStmt.setString(5,phone);    
			prepStmt.setString(6,email);  
			boolean retVal = prepStmt.execute();
			con.commit();
			return retVal;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(res!=null) {
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(prepStmt!=null) {
				try {
					prepStmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
 		return false;
	}

	public boolean checkLogin(String userName, String password) {
		Connection con = DBConnection.getConnection();
		PreparedStatement prepStmt = null;
		ResultSet res = null;
		try {
			prepStmt = con.prepareStatement("select * from user_mstr where userName=? and password=?");

			prepStmt.setString(1,userName);  
			prepStmt.setString(2,password); 
			ResultSet retVal = prepStmt.executeQuery();
			if(retVal.next()) {
				return true;
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(res!=null) {
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(prepStmt!=null) {
				try {
					prepStmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
 		return false;
	}

	public boolean doCheckout(String name, String cardNumber, String expDate, String cvc,
			String itemList) {
		Connection con = DBConnection.getConnection();
		PreparedStatement prepStmt = null;
		ResultSet res = null;
		try {
			prepStmt = con.prepareStatement("insert into order_mstr(name,card_number,expiration_date,cvc,item_list) values(?,?,?,?,?)");

			prepStmt.setString(1,name);  
			prepStmt.setString(2,cardNumber);  
			prepStmt.setString(3,expDate);  
			prepStmt.setString(4,cvc);  
			prepStmt.setString(5,itemList);    
			boolean retVal = prepStmt.execute();
			con.commit();
			return retVal;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(res!=null) {
				try {
					res.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(prepStmt!=null) {
				try {
					prepStmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
 		return false;
		
	}
}
