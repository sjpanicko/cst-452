package com.shoppingcart.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shoppingcart.db.ShoppingCartDaoImpl;

/**
 * Servlet implementation class ShoppingCrtMainServlet
 */
@WebServlet("/ShoppingCrtMainServlet")
public class ShoppingCrtMainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShoppingCrtMainServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String pageId = (String)request.getParameter("pageId");
		System.out.println("Request Obtained with method Get & page id:"+pageId);
		ShoppingCartDaoImpl cartDao = new ShoppingCartDaoImpl();
		if("register".equalsIgnoreCase(pageId)) {
			cartDao.registerUser((String)request.getParameter("username"), 
					(String)request.getParameter("pwd"), (String)request.getParameter("name"),
					(String)request.getParameter("address"), (String)request.getParameter("phone"),
					(String)request.getParameter("email"));
			response.sendRedirect("./login.jsp");
		} else 	if("login".equalsIgnoreCase(pageId)) {
			boolean retVal = cartDao.checkLogin((String)request.getParameter("username"), 
					(String)request.getParameter("pwd"));
			if(!retVal) {
				response.sendRedirect("./login.jsp?loginStatus=fail");
			} else {
				response.sendRedirect("./userMainPage.jsp");
			}
		} else 	if("checkout".equalsIgnoreCase(pageId)) {
			cartDao.doCheckout((String)request.getParameter("name"), 
					(String)request.getParameter("cardNumber"), (String)request.getParameter("expDate"),
					(String)request.getParameter("cvc"), (String)request.getParameter("itemIds"));
			response.getWriter().append("Checkout successfull ");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
