<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<style type="text/css">
    * {
        box-sizing: border-box;
    }

    .ddr-page-wrapper {
        position: relative;
        padding: 0px;
        margin: 0px;
        font-family: sans-serif;
        font-size: 16px;
    }

    .ddr-page-wrapper .ddr-page {
        position: relative;
    }

    .ddr-page-wrapper .ddr-page .ddr-page-section-pre-content::after,
    .ddr-page-wrapper .ddr-page .ddr-page-section-pre-content::before,
    .ddr-page-wrapper .ddr-page>div::after,
    .ddr-page-wrapper .ddr-page>div::before {
        content: "";
        display: table;
        clear: both;
    }

    .ddr-page-wrapper div,
    .ddr-page-wrapper h1,
    .ddr-page-wrapper h2,
    .ddr-page-wrapper h3,
    .ddr-page-wrapper h4,
    .ddr-page-wrapper h5,
    .ddr-page-wrapper h6,
    .ddr-page-wrapper p,
    .ddr-page-wrapper span {
        line-height: normal;
    }

    .ddr-page-row,
    .ddr-page-section-wrapper .ddr-page-section {
        position: relative;
        box-sizing: border-box;
    }

    .ddr-page-row {
        display: flex;
        width: 100%;
    }

    .ddr-page-row__wrap {
        flex-wrap: wrap;
    }

    .ddr-page-row__wrap .ddr-page-column__wrap {
        width: 100% !important;
    }

    .ddr-page-row__wrap-reverse {
        flex-wrap: wrap-reverse;
    }

    .ddr-page-row__wrap-reverse .ddr-page-column__wrap-reverse {
        width: 100% !important;
    }

    .ddr-page-column {
        display: flex;
        flex-wrap: wrap;
        box-sizing: border-box;
        position: relative;
        align-content: flex-start;
    }

    .ddr-page-widget {
        width: 100%;
        position: relative;
        overflow: hidden;
    }

    .ddr-widget {
        background-color: transparent;
        background-image: none;
        top: auto;
        bottom: auto;
        left: auto;
        right: auto;
        height: auto;
        width: 30%;
        max-height: none;
        max-width: none;
        min-height: 0px;
        min-width: 0px;
        padding: 0px;
        margin: 0px;
        overflow: hidden;
        margin-left: 30%;
    }

    .ddr-widget__rich-text {
        position: relative;
        cursor: auto;
        overflow-wrap: break-word;
        overflow: hidden;
    }

    .ddr-widget__image {
        position: relative;
        max-width: 100%;
        height: auto;
        margin: auto;
        display: flex;
    }

    .ddr-widget__image a,
    .ddr-widget__image img {
        overflow: hidden;
        width: 100%;
        max-width: 100%;
        max-height: 100%;
    }

    .ddr-widget__image a,
    .ddr-widget__image a:active,
    .ddr-widget__image a:focus,
    .ddr-widget__image a:hover,
    .ddr-widget__image a:link,
    .ddr-widget__image a:visited {
        display: inline-block;
        vertical-align: middle;
        color: inherit;
        text-decoration: none;
        border: none;
        box-shadow: none;
    }

    @media (min-width: 1281px) {
        .ddr-page-row__wrap--max {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--max .ddr-page-column__wrap--max {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--max {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--max .ddr-page-column__wrap-reverse--max {
            width: 100% !important;
        }

        .ddr-page-column__hide--max,
        .ddr-page-widget__hide--max {
            display: none;
        }
    }

    @media (max-width: 1280px) and (min-width: 1025px) {
        .ddr-page-row__wrap--1280 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--1280 .ddr-page-column__wrap--1280 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--1280 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--1280 .ddr-page-column__wrap-reverse--1280 {
            width: 100% !important;
        }

        .ddr-page-column__hide--1280,
        .ddr-page-widget__hide--1280 {
            display: none;
        }
    }

    @media (max-width: 1024px) and (min-width: 769px) {
        .ddr-page-row__wrap--1024 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--1024 .ddr-page-column__wrap--1024 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--1024 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--1024 .ddr-page-column__wrap-reverse--1024 {
            width: 100% !important;
        }

        .ddr-page-column__hide--1024,
        .ddr-page-widget__hide--1024 {
            display: none;
        }
    }

    @media (max-width: 768px) and (min-width: 481px) {
        .ddr-page-row__wrap--768 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--768 .ddr-page-column__wrap--768 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--768 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--768 .ddr-page-column__wrap-reverse--768 {
            width: 100% !important;
        }

        .ddr-page-column__hide--768,
        .ddr-page-widget__hide--768 {
            display: none;
        }
    }

    @media (max-width: 480px) and (min-width: 321px) {
        .ddr-page-row__wrap--480 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--480 .ddr-page-column__wrap--480 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--480 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--480 .ddr-page-column__wrap-reverse--480 {
            width: 100% !important;
        }

        .ddr-page-column__hide--480,
        .ddr-page-widget__hide--480 {
            display: none;
        }
    }

    @media (max-width: 320px) and (min-width: 0px) {
        .ddr-page-row__wrap--320 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--320 .ddr-page-column__wrap--320 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--320 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--320 .ddr-page-column__wrap-reverse--320 {
            width: 100% !important;
        }

        .ddr-page-column__hide--320,
        .ddr-page-widget__hide--320 {
            display: none;
        }
    }
</style>
<div class="ddr-page-wrapper">
        <div id="ddr-770d025f91" class="ddr-page-section-wrapper ddr-page-section-wrapper-0">
            <div class="ddr-page-section-pre-content ddr-page-section-pre-content-0">
                <div class="ddr-page-section ddr-page-section-0" style="margin-top: 100px;">
                    <div class="ddr-page-row ddr-page-row-0 ddr-page-row__wrap--320 ddr-page-row__wrap--480 ddr-page-row__wrap--768">
                        <div class="ddr-page-column ddr-page-column-0 ddr-page-column__wrap--320 ddr-page-column__wrap--480 ddr-page-column__wrap--768 " style="width: 100%;">
                            <div id="ddr-5f496af61b" class="ddr-page-widget ddr-page-widget__rich-text ddr-page-widget-0 ">
                                <div class="ddr-widget">
                                    <div id="ddr-rich-text-000" class="ddr-widget__rich-text" style="margin: 30px; border-style: solid; border-width: 0px;">
                                        <p style="text-align: center;"><span style="font-size: 26px;">Welcome To</span></p>
                                        <p style="text-align: center;"><span style="font-size: 30px; font-family: 'comic sans ms', sans-serif; color: #008000;">Diapers at your door</span></p>
                                    </div>
                                </div>
                            </div>
                            <div id="ddr-878d53cb7d" class="ddr-page-widget ddr-page-widget__image ddr-page-widget-1 ">
                                <div class="ddr-widget">
                                    <div class="ddr-widget__image" style="justify-content: center;float:right;cursor: pointer;"><a href="./register.jsp" target="_self" style="width: 117px; height: auto;"><img src="./images/register.png" sizes="117px" style="height: auto; width: 117px;"></a>
                                        <div class="ddr-hot-spots__items-wrapper"></div>
                                    </div>
                                    <div class="ddr-widget__image" style="justify-content: flex-start;float:left;cursor: pointer;"><a href="./login.jsp" target="_self" style="width: 120px; height: auto;"><img src="./images/login.png" sizes="120px" style="height: auto; width: 120px;"></a>
                                        <div class="ddr-hot-spots__items-wrapper"></div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="ddr-page-row ddr-page-row-1 ddr-page-row__wrap--320 ddr-page-row__wrap--480 ddr-page-row__wrap--768">
                        <div class="ddr-page-column ddr-page-column-0 ddr-page-column__wrap--320 ddr-page-column__wrap--480 ddr-page-column__wrap--768 " style="width: 100%;">
                            <div id="ddr-fd1766a63f" class="ddr-page-widget ddr-page-widget__image ddr-page-widget-0 ">
                                <div class="ddr-widget">
                                    <div class="ddr-widget__image" style="justify-content: center;margin-top:10px"><a target="_self" style="width: 141px; height: auto;"><img src="https://cdn.dragdropr.com/adc1905f-8a22-4811-bf98-ff775bd95def/" srcset="https://cdn.dragdropr.com/adc1905f-8a22-4811-bf98-ff775bd95def/ 95w" sizes="95px" style="height: auto; width: 141px;"></a>
                                        <div class="ddr-hot-spots__items-wrapper"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="ddr-a3f71dcf37" class="ddr-page-section-wrapper ddr-page-section-wrapper-1">
            <div class="ddr-page-section-pre-content ddr-page-section-pre-content-1">
                <div class="ddr-page-section ddr-page-section-1"></div>
            </div>
        </div>
</div>
</body>
</html>