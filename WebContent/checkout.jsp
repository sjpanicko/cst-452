<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register user</title>
</head>
<body>
<style type="text/css">
    * {
        box-sizing: border-box;
    }

    .ddr-page-wrapper {
        position: relative;
        padding: 0px;
        margin: 0px;
        font-family: sans-serif;
        font-size: 16px;
    }

    .ddr-page-wrapper .ddr-page {
        position: relative;
    }

    .ddr-page-wrapper .ddr-page .ddr-page-section-pre-content::after,
    .ddr-page-wrapper .ddr-page .ddr-page-section-pre-content::before,
    .ddr-page-wrapper .ddr-page>div::after,
    .ddr-page-wrapper .ddr-page>div::before {
        content: "";
        display: table;
        clear: both;
    }

    .ddr-page-wrapper div,
    .ddr-page-wrapper h1,
    .ddr-page-wrapper h2,
    .ddr-page-wrapper h3,
    .ddr-page-wrapper h4,
    .ddr-page-wrapper h5,
    .ddr-page-wrapper h6,
    .ddr-page-wrapper p,
    .ddr-page-wrapper span {
        line-height: normal;
    }

    .ddr-page-row,
    .ddr-page-section-wrapper .ddr-page-section {
        position: relative;
        box-sizing: border-box;
    }

    .ddr-page-row {
        display: flex;
        width: 100%;
    }

    .ddr-page-row__wrap {
        flex-wrap: wrap;
    }

    .ddr-page-row__wrap .ddr-page-column__wrap {
        width: 100% !important;
    }

    .ddr-page-row__wrap-reverse {
        flex-wrap: wrap-reverse;
    }

    .ddr-page-row__wrap-reverse .ddr-page-column__wrap-reverse {
        width: 100% !important;
    }

    .ddr-page-column {
        display: flex;
        flex-wrap: wrap;
        box-sizing: border-box;
        position: relative;
        align-content: flex-start;
    }

    .ddr-page-widget {
        width: 100%;
        position: relative;
        overflow: hidden;
    }

    .ddr-widget {
        background-color: transparent;
        background-image: none;
        top: auto;
        bottom: auto;
        left: auto;
        right: auto;
        height: auto;
        width: auto;
        max-height: none;
        max-width: none;
        min-height: 0px;
        min-width: 0px;
        padding: 0px;
        margin: 0px;
        overflow: hidden;
    }

    .ddr-widget__rich-text {
        position: relative;
        cursor: auto;
        overflow-wrap: break-word;
        overflow: hidden;
    }

    @media (min-width: 1281px) {
        .ddr-page-row__wrap--max {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--max .ddr-page-column__wrap--max {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--max {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--max .ddr-page-column__wrap-reverse--max {
            width: 100% !important;
        }

        .ddr-page-column__hide--max,
        .ddr-page-widget__hide--max {
            display: none;
        }
    }

    @media (max-width: 1280px) and (min-width: 1025px) {
        .ddr-page-row__wrap--1280 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--1280 .ddr-page-column__wrap--1280 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--1280 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--1280 .ddr-page-column__wrap-reverse--1280 {
            width: 100% !important;
        }

        .ddr-page-column__hide--1280,
        .ddr-page-widget__hide--1280 {
            display: none;
        }
    }

    @media (max-width: 1024px) and (min-width: 769px) {
        .ddr-page-row__wrap--1024 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--1024 .ddr-page-column__wrap--1024 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--1024 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--1024 .ddr-page-column__wrap-reverse--1024 {
            width: 100% !important;
        }

        .ddr-page-column__hide--1024,
        .ddr-page-widget__hide--1024 {
            display: none;
        }
    }

    @media (max-width: 768px) and (min-width: 481px) {
        .ddr-page-row__wrap--768 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--768 .ddr-page-column__wrap--768 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--768 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--768 .ddr-page-column__wrap-reverse--768 {
            width: 100% !important;
        }

        .ddr-page-column__hide--768,
        .ddr-page-widget__hide--768 {
            display: none;
        }
    }

    @media (max-width: 480px) and (min-width: 321px) {
        .ddr-page-row__wrap--480 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--480 .ddr-page-column__wrap--480 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--480 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--480 .ddr-page-column__wrap-reverse--480 {
            width: 100% !important;
        }

        .ddr-page-column__hide--480,
        .ddr-page-widget__hide--480 {
            display: none;
        }
    }

    @media (max-width: 320px) and (min-width: 0px) {
        .ddr-page-row__wrap--320 {
            flex-wrap: wrap;
        }

        .ddr-page-row__wrap--320 .ddr-page-column__wrap--320 {
            width: 100% !important;
        }

        .ddr-page-row__wrap-reverse--320 {
            flex-wrap: wrap-reverse;
        }

        .ddr-page-row__wrap-reverse--320 .ddr-page-column__wrap-reverse--320 {
            width: 100% !important;
        }

        .ddr-page-column__hide--320,
        .ddr-page-widget__hide--320 {
            display: none;
        }
    }
    
    .ddr-widget__button {
        position: relative;
        display: flex;
    }

    .ddr-widget__button a {
        overflow: hidden;
    }

    .ddr-widget__button>a>div {
        display: block;
        overflow: hidden;
        background-color: rgb(0, 143, 162);
        box-sizing: content-box;
        word-break: break-all;
        transition: all 0.5s ease 0s;
    }

    .ddr-widget__button a,
    .ddr-widget__button a:active,
    .ddr-widget__button a:focus,
    .ddr-widget__button a:hover,
    .ddr-widget__button a:link,
    .ddr-widget__button a:visited {
        display: inline-block;
        vertical-align: middle;
        color: inherit;
        text-decoration: none;
        border: none;
        box-shadow: none;
    }
    
</style>
<script>
function checkout(){
	document.checkoutForm.submit();
}
function goBack(){
	window.location.href = "./userCartPage.jsp?cartItems="+document.getElementById('itemIds').value.slice(1,document.getElementById('itemIds').value.length);
}
</script>
<form method="get" action="./ShoppingCrtMainServlet" name="checkoutForm">
<input type="hidden" id="pageId" name="pageId" value="checkout"/>
<input type="hidden" id="itemIds" name="itemIds" value="<%= request.getParameter("itemIds")%>"/>

<div class="ddr-page-wrapper">
    <div class="ddr-page">
        <div id="ddr-a3e1340682" class="ddr-page-section-wrapper ddr-page-section-wrapper-0">
            <div class="ddr-page-section-pre-content ddr-page-section-pre-content-0">
                <div class="ddr-page-section ddr-page-section-0">
                    <div class="ddr-page-row ddr-page-row-0 ddr-page-row__wrap--320 ddr-page-row__wrap--480 ddr-page-row__wrap--768">
                        <div class="ddr-page-column ddr-page-column-0 ddr-page-column__wrap--320 ddr-page-column__wrap--480 ddr-page-column__wrap--768 " style="width: 100%;">
                            <div id="ddr-1a7352637d" class="ddr-page-widget ddr-page-widget__rich-text ddr-page-widget-0 ">
                                <div class="ddr-widget">
                                    <div id="ddr-rich-text-000" class="ddr-widget__rich-text" style="margin: 30px;">
                                        <p></p>
                                        <p></p>
                                        <p style="text-align: center;"><span style="color: #0000ff; font-family: 'comic sans ms', sans-serif; font-size: 34px;">Checkout Here</span></p>
                                        <p style="text-align: center;"></p>
                                        <p style="text-align: center;"><span style="color: #ff6600; font-family: 'comic sans ms', sans-serif; font-size: 26px;">Name:&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="name" name="name" style="height:25px;width:200px;border-style: solid;"/></span></p>
                                        <p style="text-align: center;"><span style="color: #ff6600; font-family: 'comic sans ms', sans-serif; font-size: 26px;">Card Number:&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<input type="text" id="cardNumber" name="cardNumber" style="height:25px;width:200px;border-style: solid;"/></span></p>
                                        <p style="text-align: center;"><span style="color: #ff6600; font-family: 'comic sans ms', sans-serif; font-size: 26px;">Expiration Date:&nbsp;  <input type="text" id="expDate" name="expDate" style="height:25px;width:200px;border-style: solid;"/></span></p>
                                        <p style="text-align: center;"><span style="color: #ff6600; font-family: 'comic sans ms', sans-serif; font-size: 26px;">CVC:&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="cvc" name="cvc" style="height:25px;width:200px;border-style: solid;"/></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="ddr-page-row ddr-page-row-1 ddr-page-row__wrap--320 ddr-page-row__wrap--480 ddr-page-row__wrap--768">
            <div class="ddr-page-column ddr-page-column-0 ddr-page-column__wrap--320 ddr-page-column__wrap--480 ddr-page-column__wrap--768 " style="width: 100%;">
                <div id="ddr-3400d76e58" class="ddr-page-widget ddr-page-widget__button ddr-page-widget-0 " data-ddr-page-widget-additional-css-id="ad10c75fc5">
                    <div class="ddr-widget">
                        <div class="ddr-widget__button" style="justify-content: center;float:left;margin-left:30%"><a target="_self" style="margin: 30px; border-radius: 3px; border-color: rgb(128, 235, 19); border-style: solid; border-width: 5px;">
                                <div id="button-100" style="padding: 10px 15px; font-size: 18px; font-weight: bold;cursor: pointer;" onclick="goBack()">Back</div>
                            </a></div>
                        <div class="ddr-widget__button" style="justify-content: center;float:right;margin-right:30%"><a target="_self" style="margin: 30px; border-radius: 3px; border-color: rgb(245, 166, 35); border-style: solid; border-width: 5px;">
                                <div id="button-101" style="padding: 10px 15px; font-size: 18px; font-weight: bold;cursor: pointer;" onclick="checkout();">Checkout</div>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
</body>
</html>