# CST-452

Diapers at your Door version 1.0 09/08/2019

Problem being solved: 
The purpose of this project is to create a user-friendly online store that will allow parents to join a reliable, affordable, and convenient diaper subscription service. The ultimate goal of Diapers at your Door is to provide parents with a reliable and convenient tool to help accommodate their children’s needs. The diaper subscription will give parents the opportunity to order diapers per one box and receive 1-day delivery or to sign up for a weekly, biweekly, or monthly subscription utilizing the website and allowing parents to pick one diaper brand or mismatch up to 3 different brands. All this easily accessible from their child’s nursery. Parents are often caught off guard when they see the last diaper in the changing table. Diapers at your Door puts an end to this. Diapers at your door guarantees delivery on expected date and parents have the option to pick every 3 days, weekly, biweekly, or monthly delivery.  No longer will parents have to rush their little one to the store to pick up diapers, as they will always be available on Diapers at your Door. Convenience is grace God has provided humanity with. Parents should be allowed to receive and enjoy the convenience of helpful tools.

Installation:
Step 1: Install the Eclipse program to your desktop. 
Step 2: Import the shopping-cart project into the Eclipse program.  
Step 3: Load the exported shoppingcart.sql to MySQL. 
Step 4: Run the Diapers at your Door application. 

Approach to implementation: 
•	To create a fully functioning ecommerce's web site that sells diapers. 
•	Properly secure and create an application and database with the intent to prevent hacking and information leaks of 		sensitive material.
•	Create a database driven web-based application that utilizes business rules, keys, and constraints within the database to improve serviceability.
•	Increasing the efficiency, scalability, and reusability of the application and its database.
•	Creating an application that allows users to create an account and view different diaper brands and sizes they can purchase. 
•	Have a product that a customer can see a picture, description, and price.
